package com.javapointers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import thymeleaf.login.form.LoginForm;
@Controller

public class LoginController {
	@RequestMapping(value="/login", method=RequestMethod.GET)
     public String getLoginForm() {
		return "login";
		
     }
	@RequestMapping(value="/login", method=RequestMethod.POST)
	public String login(@ModelAttribute(value="loginForm") LoginForm loginForm, Model model) {
		
		String Email	= loginForm.getEmail();
		String password = loginForm.getPassword();
		
		if("demo@bumin.com.tr".equals (Email) && "cjaiU8CV".equals (password)) {
			return "home";
			
		}
		model.addAttribute("invalidCredentials", true);
		return "login";
		
	}
}
