package com.javapointers;


import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import javax.net.ssl.HttpsURLConnection;
import com.google.gson.JsonObject;
import org.json.JSONObject;

public class  TransactionReportRequest {
	private  HttpsURLConnection con;
	         
    public  void postRequest(String fromDate, String toDate, String merchant, String acquirer) 
    {

       
        String url1 = "https://sandbox-reporting.rpdpymnt.com/api/v3/transactions/report";
        
        JSONObject requestBody1 = new JSONObject();
        try {
            requestBody1.put("fromDate", fromDate);
            requestBody1.put("toDate", toDate);
            requestBody1.put("merchant", 1);
            requestBody1.put("acquirer", 2);
        } 
        catch (Exception e){
           
        }
        try {

            URL myurl = new URL(url1);
            con = (HttpsURLConnection) myurl.openConnection();

            con.setDoOutput(true);
            con.setDoInput (true);
            con.setRequestMethod("POST");          
            con.setRequestProperty("Content-Type", "application/json");
            con.setRequestProperty("Authorization", "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJtZXJjaGFudFVzZXJJZCI6NTMsInJvbGUiOiJ1c2VyIiwibWVyY2hhbnRJZCI6Mywic3ViTWVyY2hhbnRJZHMiOlszLDc0LDkzLDExOTEsMTI5NSwxMTEsMTM3LDEzOCwxNDIsMTQ1LDE0NiwxNTMsMzM0LDE3NSwxODQsMjIwLDIyMSwyMjIsMjIzLDI5NCwzMjIsMzIzLDMyNywzMjksMzMwLDM0OSwzOTAsMzkxLDQ1NSw0NTYsNDc5LDQ4OCw1NjMsMTE0OSw1NzAsMTEzOCwxMTU2LDExNTcsMTE1OCwxMTc5LDEyOTMsMTI5NF0sInRpbWVzdGFtcCI6MTUzODMyNjQ0NH0.WCz8auxY5jMtksVzTxyslL3BYckwOnujeBztfvxt35s");
            DataOutputStream wr = new DataOutputStream(con.getOutputStream());
            
            wr.writeBytes(requestBody1.toString());
          
            StringBuilder content;

            try (BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()))) {

                String line;
                content = new StringBuilder();

                while ((line = in.readLine()) != null) {
                    content.append(line);
                    content.append(System.lineSeparator());
                }
            }

            System.out.println(content.toString());

        } catch(Exception e) 
        
        {
        	System.out.println(e.toString());
        }
        finally {
            
            con.disconnect();
        }
    }
    
    
}
