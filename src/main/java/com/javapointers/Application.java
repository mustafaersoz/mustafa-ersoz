package com.javapointers;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.ProtocolException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Jerry Conde, webmaster@javapointers.com
 * @since 8/9/2016
 */

public class Application {

    public static void main(String[] args) {
        // SpringApplication.run(Application.class, args);
        LoginRequest request=new LoginRequest();
        request.postRequest();
        
        //TransactionReportRequest request=new TransactionReportRequest();
        //request.postRequest("2015-07-01","2015-10-01" , "","");
    }
}
